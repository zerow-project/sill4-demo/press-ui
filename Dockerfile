FROM node:14-alpine as builder

WORKDIR /app
COPY package* ./
RUN npm install
COPY . .
RUN npm run build:vite

FROM nginx

ADD default.conf.template /etc/nginx/conf.d/default.conf.template

CMD ["/bin/bash", "-c", "envsubst '$API_BACKEND' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'"]

COPY --from=builder /app/dist /app
